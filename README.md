# Game Closure Devkit Plugin : Admob

## Demo
Check out [the demo application](https://github.com).


## Installation
Install the module using using the standard devkit install process:

~~~
devkit install https://github.com/chung-nguyen/gameclosure-admob.git
~~~


## Usage
TBD

